import test from 'ava';
import nupackMarkup from './';

test('should produce the correct estimate given all parameters (ex. 1)', t => {
  t.is(nupackMarkup(1299.99, 3, 'food'), 1591.58);
});

test('should produce the correct estimate given all parameters (ex. 2)', t => {
  t.is(nupackMarkup(5432.00, 1, 'drugs'), 6199.81);
});

test('should produce the correct estimate given all parameters (ex. 3)', t => {
  t.is(nupackMarkup(12456.95, 4, 'books'), 13707.63);
});

test('should normalize case on type', t => {
  t.is(nupackMarkup(1299.99, 3, 'FOOD'), 1591.58);
});

test('should handle "pharmaceuticals" as drugs', t => {
  t.is(nupackMarkup(5432.00, 1, 'pharmaceuticals'), 6199.81);
});

test('should allow an object with reasonable keys to be passed in', t => {
  const params = {
    basePrice: 12456.95,
    staffRequired: 4,
    type: 'books'
  };
  t.is(nupackMarkup(params), 13707.63);
});

test('should throw an error when the type is not specified', t => {
  t.throws(() => nupackMarkup(12456.95, 4), /type must be specified/);
});

test('should throw an error when the type property is not given in the object', t => {
  t.throws(
    () => nupackMarkup({basePrice: 12456.95, staffRequired: 4}),
    /type must be specified/
  );
});

test.failing('should throw an error when base price is not a positive number', t => {
  t.throws(
    () => nupackMarkup('read', 4, 'books'),
    /base price must be a positive number/
  );
});

test.failing('should throw an error when a decimal is specified for staff', t => {
  t.throws(
    () => nupackMarkup(5432.00, 1.5, 'drugs'),
    /number of staff must be a positive integer/
  );
});
