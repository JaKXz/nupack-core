'use strict';

const DEFAULT_MARKUP_RATE = 0;
const DRUGS_MARKUP_RATE = 0.075;
const ELECTRONICS_MARKUP_RATE = 0.02;
const FLAT_MARKUP = 0.05;
const FOOD_MARKUP_RATE = 0.13;
const MARKUP_RATE_PER_STAFF = 0.012;

module.exports = function nupackMarkup() {
  if (arguments.length === 1) {
    const {
      basePrice,
      staffRequired,
      type
    } = arguments[0];
    return calculateMarkup(basePrice, staffRequired, type);
  }
  return calculateMarkup(...arguments);
};

function calculateMarkup(baseCost, numStaff, type = mandatory('type')) {
  baseCost += baseCost * FLAT_MARKUP;
  const result = baseCost +
    baseCost * getMarkupRateForStaff(numStaff) +
    baseCost * getMarkupRateForType(type.toLowerCase());
  return Number(result.toFixed(2));
}

function getMarkupRateForType(type) {
  return {
    drugs: DRUGS_MARKUP_RATE,
    electronics: ELECTRONICS_MARKUP_RATE,
    food: FOOD_MARKUP_RATE,
    pharmaceuticals: DRUGS_MARKUP_RATE
  }[type] || DEFAULT_MARKUP_RATE;
}

function getMarkupRateForStaff(numStaff) {
  return numStaff * MARKUP_RATE_PER_STAFF;
}

function mandatory(paramName) {
  throw new Error(`${paramName} must be specified`);
}
