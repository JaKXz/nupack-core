# NuPack Core [![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/sindresorhus/xo) [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) [![npm version](https://badge.fury.io/js/%40jakxz%2Fnupack-core.svg)](https://badge.fury.io/js/%40jakxz%2Fnupack-core)
> APIs for NuPack!

## Getting Started
```bash
brew install node # or install v6+ from https://nodejs.org
git clone git@gitlab.com:JaKXz/nupack-core.git
cd nupack-core
npm install
npm test
```

## Development
```bash
npm run tdd
# OR
npm install -g ava
ava --watch
```

### Testing Frameworks Used
- [ava](http://ava.li) - parallel test runner (I've helped shape some of the features and contributed bug reports)
- [xo](http://npm.im/xo) - customizable eslint runner (I've made PRs to this one!)
  - +[eslint-plugin-ava](http://npm.im/eslint-plugin-ava)
- [nyc](http://npm.im/nyc) - code coverage tool based on [istanbul](http://npm.im/istanbul) (I'm a collaborator on this one! :smile:)

## **Pricing problem**

NuPack is responsible for taking existing products and repackaging them for sale at electronic stores like Best Buy. Companies will phone up NuPack, explain the process and NuPack needs to quickly give them an estimate of how much it will cost. Different markups to the job:

* Without exception, there is a flat markup on all jobs of 5%
* For each person that needs to work on the job, there is a markup of 1.2%

Markups are also added depending on the types of materials involved:

* If pharmaceuticals are involved, there is an immediate 7.5% markup
* For food, there is a 13% markup
* Electronics require a 2% markup
* Everything else, there is no markup

Another system calculates the base price depending on how many products need to be repackaged. As such, the markup calculator should accept the initial base price along with the different categories of markups and calculate a final cost for a project.

The flat markup is calculated first and then all other markups are calculated on top of the base price plus flat markup.

### Example 1:

    Input:  $1,299.99, 3 people, food
    Output: $1,591.58

### Example 2:

    Input:  $5,432.00, 1 person, drugs
    Output: $6,199.81

### Example 3:

    Input:  $12,456.95, 4 people, books
    Output: $13,707.63
